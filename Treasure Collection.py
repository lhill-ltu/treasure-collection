import time
import networkx as nx
import matplotlib.pyplot as plt

# creating dictionary of cities for the pre-order DFS

graphPreOrder = {'9': ['5', '14'],
'5': ['9', '3', '6'],
'3': ['1', '4', '5'],
'6': ['5'],
'1': ['3'],
'4': ['3'],
'14': ['9', '11', '17'],
'11': ['10', '12', '14'],
'17': ['15', '19', '14'],
'10': ['11'],
'12': ['11'],
'15': ['17'],
'19': ['17']}

# Creating adjacency matrix for the cities

newGraph = nx.Graph(graphPreOrder)
adjacencyMatrix = nx.adjacency_matrix(newGraph)
adjacencyMatrixTable = adjacencyMatrix.todense() # Converting the adjacency matrix to its table form for easier reading

# Making a visible graph of the cities by creating edges between the nodes

visibleGraph = nx.Graph()
visibleGraph.add_edges_from([
    ("Leeds: D", "Monrovia: D"),
    ("Leeds: D", "London: D"),
    ("Monrovia: D", "Madrid: D"),
    ("Monrovia: D", "Warsaw: D"),
    ("Madrid: D", "Rome: D"),
    ("Madrid: D", "Tirana: D"),
    ("London: D", "Paris: D"),
    ("London: D", "New York: D"),
    ("Paris: D", "Islamabad: D"),
    ("Paris: D", "Berlin: D"),
    ("New York: D", "Wash. DC: D"),
    ("New York: D", "Tokyo: D")
])

# Set the positions of the cities on the graph for the visualisation 

pos = {
    "Leeds: D": (0, 0),
    "Monrovia: D": (-2, -0.5),
    "London: D": (1, -0.5),
    "Madrid: D": (-3, -1),
    "Warsaw: D": (-1, -1.5),
    "Rome: D": (-5, -1.5),
    "Tirana: D": (-4, -2),
    "Paris: D": (1, -1),
    "Islamabad: D": (0.8, -1.8),
    "Berlin: D": (3, -2),
    "New York: D": (6, -1.5),
    "Wash. DC: D": (6, -2),
    "Tokyo: D": (8, -2)
}

# For the change in colour for root node
root = {
    "Leeds: D": (0, 0)
}

# Initialising the variables

menuState = 0
global treasures
treasures = None
visitedNode = None

# Create a node class for the in-order and post-order DFS

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


# Initialising the nodes for the in-order and post-order DFS

rootNode = Node(9)
rootNode.left = Node(5)
rootNode.right = Node(14)
rootNode.left.left = Node(3)
rootNode.left.right = Node(6)
rootNode.left.left.left = Node(1)
rootNode.left.left.right = Node(4)
rootNode.right.left = Node(11)
rootNode.right.right = Node(17)
rootNode.right.left.left = Node(10)
rootNode.right.left.right = Node(12)
rootNode.right.right.left = Node(15)
rootNode.right.right.right = Node(19)

# Creating a function to do preorder DFS using nodes

def preorderDFS(visitedNode, graph, rootNode, treasures):
    if visitedNode is None: 
        visitedNode = []
    if treasures is None: #Initialising the treasures as a integer
        treasures = 0
    visitedNode.append(rootNode) # Adding the root node to the list of visitedNodes
    treasures += 1 # Incrementing treasures by 1 for each visited nodes only
    for childNode in graph[rootNode]: # For the child nodes of the root node
        if childNode not in visitedNode: 
            visitedNode, treasures = preorderDFS(visitedNode, graph, childNode, treasures) # Let the child node be the root node and repeat the process
    return visitedNode, treasures

# Create a function to do inorder DFS using nodes

def inorderDFS(rootNode, treasures):
    if treasures is None:
        treasures = [0] # Initialising the treasures as a list so that it can be passed by reference to keep the integrity of the value when exiting a recurisve functions

    if rootNode:

        # Traverse down the left route, and since this is input first, it will travel down all possible left routes until there are no more
        inorderDFS(rootNode.left, treasures)

        # Once there are no more left routes, it will print the data of the node
        treasures[0] += 1
        print(rootNode.data)

        # Then it will attempt to go right, and if there is no right route, it will go back up the tree and try to go right again
        inorderDFS(rootNode.right, treasures)

    return treasures

# Create a function to do postorder DFS using nodes

def postorderDFS(rootNode, treasures):
    if treasures is None:
        treasures = [0]

    if rootNode:

        # Traverse down the left route, and since this is input first, it will travel down all possible left routes until there are no more
        postorderDFS(rootNode.left, treasures)

        # Then it will attempt to go right, and if there is no right route, it will go back up the tree and try to go right again
        postorderDFS(rootNode.right, treasures)

        # Once there are no more left or right routes, it will print the data of the node
        treasures[0] += 1
        print(rootNode.data)
    
    return treasures


# Explaining the program to the user

print("Hello, welcome to the treasure collection game!")
time.sleep(1)
print("You are a treasure hunter and you have to collect all the treasures in the cities")
time.sleep(1)
print("Please select what you would like the program to do :D")
time.sleep(1)

#Validating the user input

while menuState == 0:
    try:
        # Asking the user what they would like to do
        print("What would you like to do")
        print("1. Preorder")
        print("2. Inorder")
        print("3. Postorder")
        print("4. Show graph")
        print("5. Show adjacency matrix")
        menuState = int(input("Please enter a number: "))
        if menuState >= 1 and menuState <= 5:
            break
        else:
            print("Please enter a valid number")
    except ValueError:
        print("Please enter a valid number")

# Conduct the selected action

if menuState == 1: # Pre-Order DFS
    startTime = time.perf_counter() # Start the timer
    visitedNodes, treasures = preorderDFS(visitedNode, graphPreOrder, '9', treasures) # Define visitedNodes and treasures as the returns of the function
    endTime = time.perf_counter() # End the timer
    totalTime = (endTime - startTime) * 1000 # Calculating total time taken to run the function in milliseconds
    print("The treasure collection path is: ")
    print(visitedNodes) 
    print("The number of treasures collected is: " + str(treasures))
    print("The time elapsed is: " + str(totalTime) + " milliseconds")

if menuState == 2: # In-Order DFS
    print("The treasure collection path is: ")
    startTime = time.perf_counter()
    inorderDFS(rootNode, treasures) # This function does not return the order of visited nodes, so just running the function will print out the correct order
    endTime = time.perf_counter()
    totalTime = (endTime - startTime) * 1000
    treasures = inorderDFS(rootNode, treasures) 
    print("The number of treasures collected is: " + str(treasures[0]))
    print("The time elapsed is: " + str(totalTime) + " milliseconds")

if menuState == 3: # Post-Order DFS
    print("The treasure collection path is: ")
    startTime = time.perf_counter()
    postorderDFS(rootNode, treasures) # This function does not return the order of visited nodes, so just running the function will print out the correct order 
    endTime = time.perf_counter()
    totalTime = (endTime - startTime) * 1000
    treasures = postorderDFS(rootNode, treasures)
    print("The number of treasures collected is: " + str(treasures[0]))
    print("The time elapsed is: " + str(totalTime) + " milliseconds")

if menuState == 4: # Show graph
    print("The graph of the cities is: ")
    nx.draw(visibleGraph, pos, node_size = 2000, font_size = 7, with_labels=True) # Drawing the graph with set node size, font and with the labels of the cities on the nodes
    nx.draw_networkx_nodes(visibleGraph, pos, root, node_size = 2000, node_color='r') #Drawing the root node, Leeds, in red for the user to easily see
    plt.show()

if menuState == 5: # Show adjacency matrix
    print("The adjacency matrix of the graph is: ")
    print(adjacencyMatrixTable)